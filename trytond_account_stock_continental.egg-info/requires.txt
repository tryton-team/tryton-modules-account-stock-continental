trytond_account<7.1,>=7.0
trytond_account_product<7.1,>=7.0
trytond_stock<7.1,>=7.0
trytond<7.1,>=7.0

[test]
proteus<7.1,>=7.0
trytond_sale<7.1,>=7.0
trytond_purchase<7.1,>=7.0
trytond_account_invoice<7.1,>=7.0
trytond_sale_supply_drop_shipment<7.1,>=7.0
